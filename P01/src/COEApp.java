import java.util.ArrayList;

public class COEApp {

	public Bid findBidWithHighestPremium(ArrayList<Bid> bids) {
		// TODO: P01 Task 4 - Calculate and return the Bid with Highest COE Premium
		Bid highestPremium = bids.get(0);
		for (Bid loopList: bids){
			if (highestPremium.getPremium() < loopList.getPremium()){
				highestPremium = loopList;
			}
		}
		return highestPremium;
	}

	public Bid findBidWithLowestPremium(ArrayList<Bid> bids) {
		// TODO: P01 Task 5 - Calculate and return the Bid with Lowest COE Premium
		Bid lowestPremium = bids.get(0);
		for (Bid loopList: bids){
			if (lowestPremium.getPremium() > loopList.getPremium()){
				lowestPremium = loopList;
			}
		}
		return lowestPremium;
	}

	public double calcTotalPremium(ArrayList<Bid> bids) {
		// TODO: P01 Task 6 - Calculate and return Total COE Premium
		double totalPremium = 0;
		for (Bid looplist : bids){
			totalPremium += looplist.getPremium();
		}
		return totalPremium;
	}

	public Bid findBid(ArrayList<Bid> bids, int year, String month, int round) {
		// TODO: P01 Task 7 - Find and return Bid for specified info
		Bid findBid = null;
		for (Bid loopList: bids){
			if (loopList.getYear() == year && loopList.getMonth().equalsIgnoreCase(month) && loopList.getRound() == round){
				findBid = loopList;
			}
			/*if (loopList.isThisBid(year, month, round)){
				findBid = loopList;
				break;
			}*/
		}
		return findBid;
	}

	public ArrayList<Bid> findGreatestDifferenceInPremium(ArrayList<Bid> bids) {
		ArrayList<Bid> differencePremium = new ArrayList<Bid>();
		double maxDiff=0;
			
		for (Bid bid1 : bids){
			double diff=0;
			Bid bid2=null;
			if (bid1.getRound()==1){
				bid2=findBid(bids, bid1.getYear(), bid1.getMonth(), 2);
			}
			if(bid2!=null){
				diff=Math.abs(bid1.getPremium()-bid2.getPremium());
								
				if(diff>maxDiff){
					maxDiff=diff;
					differencePremium.clear();
					differencePremium.add(bid1);
					differencePremium.add(bid2);
				}
			}
		}
		return differencePremium;
	}
	
	/*public double averageEachMonth(ArrayList<Bid> bids) {
		String[] monthArray = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		int[] yearArray = {2007, 2008, 2009};
		
		double totalEachMonth = 0;
		for (Bid looplist : bids){
			
		}
		return totalPremium;
	}*/
	
	public ArrayList<Bid> findMonth(ArrayList<Bid> bids) {
		ArrayList<Bid> findMonth = new ArrayList<Bid>();
		for (Bid loopList: bids){
			if (loopList.getPremium() > 10000){
				findMonth.add(loopList);
			}
		}
		return findMonth;
	}

	public ArrayList<Bid> filterBidsByMonth(ArrayList<Bid> bids, String month) {
		ArrayList<Bid> filterByMonth = new ArrayList<Bid>();
		for (Bid loopList: bids){
			if (loopList.getMonth().equalsIgnoreCase(month)){
				filterByMonth.add(loopList);
			}
		}
		return filterByMonth;
	}
}
