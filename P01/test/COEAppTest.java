import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class COEAppTest {
	private Bid firstBid, secondBid, thirdBid;
	private ArrayList<Bid> bids, empty;
	private COEApp app;


	@Before
	public void setup() {
		firstBid = new Bid("CatA", 2011, "Oct", 1, 2000, 50, 70000);
		secondBid = new Bid("CatA", 2011, "Nov", 1, 1000, 50, 80000);
		thirdBid = new Bid("CatA", 2011, "Oct", 1, 1500, 50, 50000);

		bids = new ArrayList<Bid>();
		bids.add(firstBid);
		bids.add(secondBid);
		bids.add(thirdBid);

		empty = new ArrayList<Bid>();

		app = new COEApp();
	}

	@Test
	public void testFindBidWithHighestPremium() {
		Bid result = app.findBidWithHighestPremium(bids);
		assertThat(secondBid, is(result));
	}

	@Test
	public void testFindBidWithHighestPremiumWithEmptyBids() {
		Bid result = app.findBidWithHighestPremium(empty);
		assertNull(result);
	}


	@Test
	public void testFindBidWithLowestPremium() {
		Bid result = app.findBidWithLowestPremium(bids);
		assertThat(thirdBid, is(result));
	}

	@Test
	public void testFindBidWithLowestPremiumWithEmptyBids() {
		Bid result = app.findBidWithLowestPremium(empty);
		assertNull(result);
	}

	@Test
	public void testCalcTotalPremium() {
		double result = app.calcTotalPremium(bids);
		assertEquals(200000, result, 0.01);
	}

	@Test
	public void testCalcTotalPremiumWithEmptyBids() {
		double result = app.calcTotalPremium(empty);
		assertEquals(0, result, 0.01);
	}

	@Test
	public void testFindBidSuccess() {
		Bid result = app.findBid(bids, 2011, "Nov", 1);
		assertThat(secondBid, is(result));
	}

	@Test
	public void testFindBidFail() {
		Bid result = app.findBid(empty, 2011, "Dec", 1);
		assertNull(result);
	}

	@Test
	public void testFilterBidsByMonth() {
		ArrayList<Bid> result = app.filterBidsByMonth(bids, "Oct");

		assertThat(result.size(), is(2));
		assertThat(result, hasItem(firstBid));
		assertThat(result, hasItem(thirdBid));
	}

	@Test
	public void testFindGreatestDifferenceInPremium() {
		ArrayList<Bid> result = app.findGreatestDifferenceInPremium(bids);

		assertThat(result.size(), is(2));
		assertThat(result, hasItem(secondBid));
		assertThat(result, hasItem(thirdBid));
	}

	@Test
	public void testFindGreatestDifferenceInPremiumWithEmptyBid() {
		ArrayList<Bid> result = app.findGreatestDifferenceInPremium(empty);

		assertNull(result);
	}
}
